from ConfigParser import SafeConfigParser
from ast import literal_eval

conf = SafeConfigParser()
conf.read('default')

elmt_temps = conf.sections()
if 'default' in elmt_temps:
    elmt_temps.remove('default')

elements = ['sun', 'tree', 'cold', 'moon', 'wild', 'tame']

for element in elements:
    if element not in elmt_temps:
        print("Warning! element %s not found in config" % element)
        continue
    items = conf.items(element)
    stats = [
        'spreads_near_friendly_', 
        'spreads_near_enemy_', 
        'spreads_near_both_',
        'dies_near_friendly_',
        'dies_near_enemy_',
        'dies_near_both_',
        'always_dies',
        'converts_',
        ]
    for item in items:
        pass
